import base64url from 'base64url'

export default {
    setItem (key, value) {
        localStorage.setItem(key, JSON.stringify(value))
    },
    getItem (key) {
        return JSON.parse(localStorage.getItem(key))
    },
    removeItem (key) {
        return localStorage.removeItem(key)
    },
    isLogged () {
        return typeof localStorage.getItem('id_token') === 'string'
    },
    getUser () {
        return JSON.parse(base64url.decode(this.getItem('id_token').split('.')[1])).user
    }
}
