import Vue from 'vue'
import Router from 'vue-router'

import AfterEach from './AfterEach.js'
import BeforeEach from './BeforeEach.js'

import Dashboard from '@/foundation/welcome/Dashboard.vue'

import Auth from '@/foundation/auth'
import AccountGroup from '@/app/account-group'
import Balance from '@/app/balance'
import Calendar from '@/app/calendar'
import ManageImports from '@/app/manage-import'
import Purchaser from '@/app/purchaser'
import Reports from '@/app/reports'

Vue.use(Router)

const router = new Router({
    routes: [
        {
            path: '/',
            redirect: { name: 'welcome.dashboard' }
        },
        {
            path: '/dashboard',
            name: 'welcome.dashboard',
            component: Dashboard,
            meta: { requiresAuth: true }
        },
        ...Auth,
        ...AccountGroup,
        ...Balance,
        ...Calendar,
        ...ManageImports,
        ...Purchaser,
        ...Reports,
        {
            path: '*',
            redirect: '/'
        }
    ]
})

router.afterEach(AfterEach)

router.beforeEach(BeforeEach)

export default router
