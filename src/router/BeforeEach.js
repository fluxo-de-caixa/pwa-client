import Auth from '@/Auth.js'

export default (to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        if (!Auth.isLogged()) {
            next({
                path: '/login',
                query: { redirect: to.fullPath }
            })
        } else {
            Auth.setItem('last_route', from.name)
            next()
        }
    } else {
        // make sure to always call next()!
        next()
    }
}
