import Vue from 'vue'
import axios from 'axios'
import Auth from '@/Auth.js'

const http = axios.create({
    baseURL: `http://localhost:${process.env.PORT_ENV}/`,
    headers: {
        'Content-Type': 'application/json',
        'accept': '*/*'
    }
})

const request = {
    success: config => {
        if (Auth.isLogged()) config.headers.Authorization = Auth.getItem('id_token')
        return config
    },
    error: error => {
        return Promise.reject(error)
    }
}
http.interceptors.request.use(request.success, request.error)

const response = {
    success: response => {
        return response
    },
    error: error => {
        return Promise.reject(error)
    }
}
http.interceptors.response.use(response.success, response.error)

Vue.prototype.$http = http
