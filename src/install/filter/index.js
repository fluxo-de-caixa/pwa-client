import Vue from 'vue'
import moment from 'moment'
import accounting from 'accounting'

window.moment = require('moment')
moment.locale('pt-BR')

Vue.filter('formatDate', function (value) {
    if (value) {
        return moment(String(value)).format('DD/MM/YYYY')
    }
})

Vue.filter('formatDateEn', function (value) {
    if (value) {
        return moment(String(value)).format('YYYY-MM-DD')
    }
})

Vue.filter('skillCurrent', function () {
    return moment().format('YYYY.MM')
})

Vue.filter('momentFormat', function (value, formatInput = 'DD/MM/YYYY', formatOutput = 'YYYY-MM-DD') {
    if (value) {
        return moment(value, formatInput).format(formatOutput)
    }
})

Vue.filter('isDateValid', function (value, formatInput = 'DD/MM/YYYY') {
    if (value) {
        return moment(value, formatInput).isValid()
    }
})

Vue.filter('formatMoney', function (value) {
    return accounting.formatMoney(value, 'R$ ', 2, '.', ',')
})
