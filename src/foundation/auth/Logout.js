import { mapActions } from 'vuex'
import Auth from '@/Auth.js'
export default {
    template: '<div id="logout"></div>',
    name: 'AuthLogout',
    mounted () {
        this.dropSession()
    },
    methods: {
        ...mapActions([
            'setUser',
            'setIsLogged'
        ]),
        dropSession () {
            Auth.removeItem('id_token')
            Auth.removeItem('lookup')
            this.setIsLogged(false)
            this.setUser({})
            this.$router.push({ name: 'auth.login' })
        }
    }
}
