import AuthLogin from './Login.vue'
import AuthLogout from './Logout.js'
import UserForm from './User.vue'

export default [
    {
        path: '/login',
        name: 'auth.login',
        component: AuthLogin
    },
    {
        path: '/logout',
        name: 'auth.logout',
        component: AuthLogout
    },
    {
        path: '/user/store',
        name: 'user.store',
        component: UserForm
    },
    {
        path: '/user/edit/:id',
        name: 'user.update',
        component: UserForm,
        meta: { requiresAuth: true }
    }
]
