import Auth from '@/Auth.js'
export default {
    setIsLogged ({ commit }, payload) {
        commit('SET_IS_LOGGED', payload)
    },
    setUser ({ commit }, payload) {
        commit('SET_USER', payload)
    },
    check ({ dispatch, state }) {
        if (Object.keys(state.user).length > 0) {
            return true
        } else {
            let auth = Auth.getItem('auth')
            if (Object.keys(auth.user).length > 0) {
                dispatch('setUser', auth.user)
                dispatch('setIsLogged', Auth.getItem('admin'))
                return true
            } else {
                return false
            }
        }
    }
}
