export default {
    isLogged (state) {
        return state.isLogged
    },
    user (state) {
        return state.user
    }
}
