export default {
    SET_IS_LOGGED (store, payload) {
        store.isLogged = payload
    },
    SET_USER (store, payload) {
        store.user = payload
    }
}
