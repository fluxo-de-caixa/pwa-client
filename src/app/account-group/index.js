import AccountGroup from './AccountGroup.vue'

export default [
    {
        path: '/account-group',
        name: 'account.main',
        component: AccountGroup,
        meta: { requiresAuth: true }
    }
]
