import BalanceList from './List.vue'
import BalanceForm from './Form.vue'

export default [
    {
        path: '/balance',
        name: 'balance.main',
        redirect: { name: 'balance.list' },
        meta: { requiresAuth: true }
    },
    {
        path: '/balance/list',
        name: 'balance.list',
        component: BalanceList,
        meta: { requiresAuth: true }
    },
    {
        path: '/balance/store',
        name: 'balance.store',
        component: BalanceForm,
        meta: { requiresAuth: true }
    },
    {
        path: '/balance/edit/:id',
        name: 'balance.update',
        component: BalanceForm,
        meta: { requiresAuth: true }
    }
]
