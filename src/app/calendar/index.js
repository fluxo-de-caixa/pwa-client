import Calendar from './Calendar.vue'

export default [
    {
        path: '/calendar',
        name: 'calendar.main',
        component: Calendar,
        meta: { requiresAuth: true }
    }
]
