import BalanceReports from './BalanceReports.vue'

export default [
    {
        path: '/reports',
        name: 'reports.main',
        component: BalanceReports,
        meta: { requiresAuth: true }
    }
]
