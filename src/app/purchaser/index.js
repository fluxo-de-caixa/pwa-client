import Purchaser from './Purchaser.vue'

export default [
    {
        path: '/purchaser',
        name: 'purchaser.main',
        component: Purchaser,
        meta: { requiresAuth: true }
    }
]
