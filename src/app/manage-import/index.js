import ManageImports from './ManageImports.vue'

export default [
    {
        path: '/manage-imports',
        name: 'manage.imports',
        component: ManageImports,
        meta: { requiresAuth: true }
    }
]
